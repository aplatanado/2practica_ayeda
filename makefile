CXX = g++
CFLAGS = -std=c++11 -I$(IDIR) 

SRCDIR = ./src/
IDIR = ./include/

SOURCES = $(SRCDIR)*.cpp

all: clean calculadora run 

debug: gdb clean

gdb:
	gdb calculadora 

calculadora:
	$(CXX) $(CFLAGS) $(SOURCES) -o $@
	
run:
	./calculadora
	
clean:
	$(RM) calculadora

