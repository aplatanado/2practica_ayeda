#include "common.hpp"
#include "nodo_pila.hpp"
// NULL<-[x]<-[x]<-[x]<-current
// sale 		entra
template <class TDATO>
class pila_t{
  
private:
  int sz;
  nodo_pila <TDATO> *current;
  
public:
  pila_t(int tam=0, bool zero=false); //zero=false indica que se debe dar valores aleatorios a cada nodo
  ~pila_t(void);
  
  void push (TDATO);
  TDATO pop (void);
  void Mostrar(void);
  int size (void);
};

template <class TDATO>
pila_t<TDATO>::pila_t(int tam, bool zero): current(NULL), sz(0){
  nodo_pila <TDATO> *tmp=current, *tmp2; //tmp=NULL
  while (tam>0){
    tam--;
    if (zero)
      tmp=new nodo_pila <TDATO>(0);
    else 
      tmp=new nodo_pila <TDATO>((TDATO)(rand()%10)*(TDATO)1.3);
    tmp->down=current;
    current=tmp;
  }
}

template <class TDATO>
pila_t<TDATO>::~pila_t(){
  nodo_pila <TDATO> *tmp=current;
  while (current!=NULL){
    current=current->down;
    delete [] tmp;
    tmp=current;
  }
}

template <class TDATO>
void pila_t<TDATO>::push (TDATO a){
  sz++;
  nodo_pila <TDATO> *tmp;
  tmp=new nodo_pila <TDATO> (a, current);
  current=tmp;
}

template <class TDATO>
TDATO pila_t<TDATO>::pop (void){  
  if (current!=NULL){
    sz--;
    TDATO retorno;
    nodo_pila <TDATO> *tmp;
    tmp=current;
    retorno=current->a;
    current=current->down;
    delete [] tmp;
    return retorno;
  }
  cout << "Pila vacia, no se pudo sacar nada!";
  return -1;
}

template <class TDATO>
void pila_t<TDATO>::Mostrar(void){
  nodo_pila <TDATO> *tmp=current;
  while (tmp!=NULL){
    cout << "[ " << tmp->a << " ]";
    cout << "\u2192";
    tmp=tmp->down;
  }
  cout << " NULL\n";
}

template <class TDATO>
int pila_t<TDATO>::size (void){
  return sz;
}

