#pragma once
#include "common.hpp"

class racional_c{
  
private:
  int numerador;
  unsigned int denominador;
  
public:
  racional_c (int num = 1, int den = 1);
//   racional_c (void); //pide por teclado
  racional_c (const racional_c&);  
  
  void MCD (void);  
  void DivisorComun(racional_c&);
  int get_numerador(void) const; //Los usa real_c
  unsigned int get_denominador(void) const;
  void set_numerador (int);
  void set_denominador (unsigned int);
  
  const racional_c operator+ (const racional_c&);
  const racional_c operator- (const racional_c&);
  const racional_c operator* (const racional_c&);
  const racional_c operator/ (const racional_c&);
  
  racional_c& operator= (const racional_c&);
 
  bool operator< (const racional_c&);  
  bool operator> (const racional_c&);
  bool operator== (const racional_c&);
  bool operator!= (const racional_c&);
  
  friend ostream& operator<< (ostream&, const racional_c&);
  friend istream& operator>> (istream&, racional_c&);
};