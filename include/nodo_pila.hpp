template <class TDATO>
struct nodo_pila{
  nodo_pila(TDATO a=0,nodo_pila *down=NULL){
    this->a=a;
    this->down=down;
  }
  TDATO a;
  nodo_pila *down;
};