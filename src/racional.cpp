#include "racional.hpp"

racional_c::racional_c(const int numerador,const int denominador){
    if ( denominador == 0 )
      cout << "error: el denominador no puede ser cero\n";    
    else{
      this->numerador=numerador;
      this->denominador=denominador;
    }
    MCD();
}

// racional_c::racional_c(void){
//   cout << "introducir racional_c: ";
//   cin >> (*this);
//   MCD();
// } 

racional_c::racional_c(const racional_c& racional){ //Constructor de copia
  (*this)=racional;                                   //Usa operator=
}

void racional_c::MCD(void){
  int menor = (numerador < denominador) ? numerador : denominador;
  int mcd = 1;  
  if (menor < 0) menor *= -1;
  for (int i=1; i<=menor; i++)
    if (numerador%i==0 && denominador%i==0)
      mcd = i;
  denominador=denominador/mcd;
  numerador=numerador/mcd;
}

void racional_c::DivisorComun(racional_c& racional){
  racional_c dummy (racional);
  
  if ( denominador == racional.get_denominador() ) return;  
  
  racional.set_numerador(dummy.get_numerador()*denominador);  
  racional.set_denominador(dummy.get_denominador()*denominador);
  
  numerador=numerador*dummy.get_denominador();
  denominador=denominador*dummy.get_denominador();  
}

int racional_c::get_numerador(void) const{
  return numerador;
}

unsigned int racional_c::get_denominador(void) const{
  return denominador;
}

void racional_c::set_numerador(int num) {
  numerador = num;
}

void racional_c::set_denominador(unsigned int den) {
  denominador = den;
}

//+,- iguales
const racional_c racional_c::operator+ (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);
  copia.set_numerador(numerador+copia.get_numerador());
  
  copia.MCD();  
  MCD(); 
  
  return (copia);
}

const racional_c racional_c::operator- (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);
  copia.set_numerador(numerador-copia.get_numerador());
  
  copia.MCD();  
  MCD(); 
  
  return (copia);
}

const racional_c racional_c::operator* (const racional_c& racional){
  racional_c resultado;
  
  resultado.set_numerador(racional.get_numerador()*numerador);
  resultado.set_denominador(racional.get_denominador()*denominador);  
  
  resultado.MCD();
  
  return resultado;
}

const racional_c racional_c::operator/ (const racional_c& racional){
  racional_c resultado;
  
  int den1 = denominador;
  int den2 = racional.get_denominador();
  
  resultado.set_numerador(den2*numerador);  
  resultado.set_denominador(abs(racional.get_numerador()*den1));  //Overflow en denominador porque es un unsigned int
  
  if ( racional.get_numerador() < 0 )
    resultado.set_numerador( resultado.get_numerador()*-1 );
  
  resultado.MCD();
  
  return resultado;
}

racional_c& racional_c::operator= (const racional_c& racional){
  numerador=racional.get_numerador();
  denominador=racional.get_denominador();
  return (*this);
}



//==,!=,<,> iguales
bool racional_c::operator< (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);    
  bool retorno = (numerador < copia.get_numerador());  
  MCD();
  
  return retorno;
}

bool racional_c::operator> (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);    
  bool retorno = (numerador > copia.get_numerador());  
  MCD();
  
  return retorno;
}

bool racional_c::operator== (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);    
  bool retorno = (numerador == copia.get_numerador());  
  MCD();
  
  return retorno;
}

bool racional_c::operator!= (const racional_c& racional){
  racional_c copia(racional); //Creo copia no constante
  
  DivisorComun(copia);    
  bool retorno = (numerador != copia.get_numerador());  
  MCD();
  
  return retorno;
}

ostream& operator<< (ostream& os, const racional_c& racional){
  if ( racional.denominador != 1 && racional.numerador != 0)
    os << racional.numerador << "/" << racional.denominador;
  else if (racional.denominador == 0)
    os << "division 0";
  else
    os << racional.numerador; 
  
  
    
  return os;
}

istream& operator>> (istream& is, racional_c& racional){ //Formato de entrada ---> n/d
  string s;
  is >> s;
  
  size_t div = s.find("/");
  
  racional.numerador = atoi(s.substr(0, div).c_str());
  
  if (div != string::npos)    
    racional.denominador = atoi(s.substr(div+1, string::npos).c_str());
  else
    racional.denominador = 1;
}