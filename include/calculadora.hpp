#include "common.hpp"
#include "pila.hpp"

void error (char n='1'){
  cerr << "Insuficientes operandos en operacion " << n << endl;
  exit(1);
}

template<class TDATO> 
TDATO calculadora(istream& is){
  pila_t <TDATO> pila;
  
  TDATO op1, op2;
  
  int c = '0';  
  
  while (c != '='){    
    c = is.peek();
    
    pila.Mostrar();
    cout << "---" << endl;
    switch (c){
      case '+':
	if (pila.size() < 2) error('+');
	is.ignore();
	op2 = pila.pop();
	op1 = pila.pop();	
	pila.push(op1 + op2);	
	break;
	
      case '-':
	is.ignore(1);	
	if ( isdigit((int)is.peek()) ){	  
	  is >> op1;
	  op1 = op1 * -1;
	  pila.push(op1);	  
	}
	else if (pila.size() < 2) error ('-');
	else{
	op2 = pila.pop();
	op1 = pila.pop();
	pila.push(op1 - op2);	
	}
	break;
      case '/':
	if (pila.size() < 2) error('/');
	is.ignore();
	op2 = pila.pop();
	op1 = pila.pop();
	pila.push(op1 / op2);
	break;
	
      case '*':
	if (pila.size() < 2) error('*');
	is.ignore();
	op2 = pila.pop();
	op1 = pila.pop();
	pila.push(op1 * op2);
	break;
	
      case ' ':
	is.ignore(1);
	break;	
      
      default:
	if (isdigit(c)){
	  is >> op1;
	  //DEBUG
// 	  cout << op1;
	  pila.push(op1);
	}
	else is.ignore(1);
	break;
    } // Switch 
  } // While   
  pila.Mostrar();
  TDATO pop = pila.pop();
  cout << "Resultado " << pop << endl << endl;  
  pila.Mostrar();
}
